# **INTERNET OF THINGS[I O T]**
**I**nternet **O**f **T**hings commonly refers to connection of devices to the internet.IoT device means an internet connected device that can be monitored and controlled from a remote location.
### **INDUSTRIAL REVOLUTION**
![images](images.jpeg)
# **Industrial IoT**
 It is a system that includes smart sensors, automation tools, software platforms, cloud servers and applications.

#### **Working of Industrial IoT**
- sensor networks sent data to IoT gateway [act as hub between IoT devices and cloud].
- IoT gateways which recieve and transmit data to the cloud application server.
- Sophisticated application programs are developed to handle large amount of data.
- It is made accessible using a smartphone application . 
###### **Application of Industrial IoT**
> Industrial automation is one of the common application of IoT. Automation tools like PLC(Programmable Logic Control) and PAC(Programmable Automation Control) are used with smart sensors networks connected to central cloud system . Special software applications are used to analyze  this huge data.schedule maintenance and some operations can be done remotely using indutrial IoT machines can operate at harsh environment than humans .
## **INDUSTRY 3.0**
![index](index.png)
  - field devices - sensors , actuators
  - control level - PLC ,PC's ,PID used to control the machinery
  - supervisory level - SCADA(supervisory control and data acquistion) software to control operations.
  - planning level - management excutive system
  - management level - ERP
## **INDUSTRY 4.0**
Industry 4.0 means industry 3.0 connected to internet i.e getting data from controllers and converting it into protocols in cloud
![index1](index1.png)

communication protocols of industry 3.0 - modbus,profinet  
communication protocols of industry 4.0 - MQtt,CoAp,HTtp  
Advantage of industry 4.0 is project maintenance and predictive maintenance.

### **Difference between industry 3.0 and industry 4.0**
In industry 3.0 automation processes using logic processors and information technology.These processes often operate largely without human interference ,but there is still a human aspect behind it.where industry 4.0 comes in is with the availability and use of vast quantities of data on the production floor.  

![index2](index2.png)
##### Challenges in conversion industry 3.0 to industry 4.0 protocols
- Expensive hardware
- Lack of documentation
- Properitary PLC protocols
#### Problems with industry 4.0 upgrade
1. Cost
- It is expensive for factory owners to switch  
2. Downtime
- change hardware result in downtime and nobody want to face such loss.

#### soluions 
Getting data from industry 3.0 devices/meters/sensors without changesto the original device.And then send the data to the cloud using industry 4.0 devices.
Covert   industry 3.0 protocols to industry 4.0 protocols
#### For making your own Industrial IoT
1. Identify most popular industry 3.0 devices.
2. Study protocols that these device communicate.
3. Get data from the industry 3.0 devices.
4. Send the data to cloud for Industrial 4.0. 

 - **IoT Tools** Data in IoT are stored in Time Series Data Base.
 - **IoT dashboard** To view data in dashboard usind Grafana, Thingsboard.
 - **IoT platforms** - Analyse the data using these platforms AWS IoT,google IoT,Azure IoT,Thingsboard.
 - **Alerts** - get alerts based on the data in this platforms like Zaiper,Twilio.
